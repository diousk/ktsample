package com.example.chenweiming.mykktapplication

import androidx.work.Configuration
import androidx.work.WorkManager
import androidx.work.WorkerFactory
import com.example.chenweiming.mykktapplication.di.DaggerAppComponent
import com.example.chenweiming.mykktapplication.initializers.AppInitializers
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by david.chen@soocii.me on 2018/11/8.
 */
class App: DaggerApplication() {
    @Inject lateinit var appInitializers: AppInitializers

    companion object {
        lateinit var instance: App
    }
    override fun onCreate() {
        super.onCreate()
        instance = this
        appInitializers.init(this)
        configureWorkManager()
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        DaggerAppComponent.builder().create(this)
        return DaggerAppComponent.builder().create(this)
    }

    @Inject
    lateinit var workerFactory: WorkerFactory

    private fun configureWorkManager() {
        val config = Configuration.Builder()
            .setWorkerFactory(workerFactory)
            .build()

        WorkManager.initialize(this, config)
    }
}