package com.example.chenweiming.mykktapplication.bus

import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay

enum class Status { CONNECTED, DISCONNECTED, CONNECTING }

object NetworkStatusUpdater {
    var relay: Relay<Status>
    init {
        relay = PublishRelay.create<Status>().toSerialized()
    }
}