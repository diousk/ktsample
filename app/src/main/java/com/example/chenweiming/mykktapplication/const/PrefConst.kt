package com.example.chenweiming.mykktapplication.const

class PrefConst {
    object App {
        const val PREF_NAME = "app"
        const val DEBUGGABLE = "app.debuggable"
    }
    object Auth {
        const val PREF_NAME = "auth"
        const val TOKEN = "auth.token"
    }
}