package com.example.chenweiming.mykktapplication.di

import com.example.chenweiming.mykktapplication.App
import com.example.chenweiming.mykktapplication.di.builder.ActivityBuilder
import com.example.chenweiming.mykktapplication.di.builder.ViewModelBuilder
import com.example.chenweiming.mykktapplication.worker.MyWroker
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * Created by david.chen@soocii.me on 2018/11/8.
 */
@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    ActivityBuilder::class,
    ViewModelBuilder::class,
    NetworkModule::class,
    RepoModule::class,
    AppModule::class
])
interface AppComponent: AndroidInjector<App> {

    @Component.Builder
    abstract class Builder: AndroidInjector.Builder<App>()

    fun injectMyWorker(worker: MyWroker)
}