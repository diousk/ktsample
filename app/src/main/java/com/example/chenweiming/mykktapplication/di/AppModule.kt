package com.example.chenweiming.mykktapplication.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.work.WorkerFactory
import com.example.chenweiming.mykktapplication.App
import com.example.chenweiming.mykktapplication.const.PrefConst
import com.example.chenweiming.mykktapplication.extension.PreferenceExtension
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by david.chen@soocii.me on 2018/11/8.
 */
@Module(includes = [AppModuleBinds::class])
class AppModule {
    @Provides
    fun provideContext(application: App): Context = application.applicationContext

    @Named(PrefConst.App.PREF_NAME)
    @Provides
    @Singleton
    fun provideAppPrefs(application: App): SharedPreferences {
        return PreferenceExtension.appPrefs(application)
    }

    @Named(PrefConst.Auth.PREF_NAME)
    @Provides
    @Singleton
    fun provideAuthPrefs(application: App): SharedPreferences {
        return PreferenceExtension.customPrefs(application, PrefConst.Auth.PREF_NAME)
    }

    @Provides
    @Singleton
    fun workerFactory(@Named(PrefConst.App.PREF_NAME) foo: SharedPreferences): WorkerFactory {
        return DaggerWorkerFactory(foo)
    }
}