package com.example.chenweiming.mykktapplication.di

import com.example.chenweiming.mykktapplication.initializers.AppInitializer
import com.example.chenweiming.mykktapplication.initializers.FrescoInitializer
import com.example.chenweiming.mykktapplication.initializers.LeakCanaryUtil
import com.example.chenweiming.mykktapplication.initializers.TimberInitializer
import com.example.chenweiming.mykktapplication.utils.logger.Logger
import com.example.chenweiming.mykktapplication.utils.logger.TimberLogger
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoSet
import javax.inject.Singleton

/**
 * Created by david.chen@soocii.me on 2018/11/8.
 */
@Module
abstract class AppModuleBinds {
    @Singleton
    @Binds
    abstract fun provideLogger(logger: TimberLogger): Logger

    @Binds
    @IntoSet
    abstract fun provideTimberInitializer(binds: TimberInitializer): AppInitializer

    @Binds
    @IntoSet
    abstract fun provideLeakCanaryUtil(binds: LeakCanaryUtil): AppInitializer

    @Binds
    @IntoSet
    abstract fun provideFrescoInitializer(binds: FrescoInitializer): AppInitializer
}