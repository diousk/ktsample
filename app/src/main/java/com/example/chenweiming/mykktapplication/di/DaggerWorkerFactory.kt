package com.example.chenweiming.mykktapplication.di

import android.content.Context
import android.content.SharedPreferences
import androidx.work.ListenableWorker
import androidx.work.Worker
import androidx.work.WorkerFactory
import androidx.work.WorkerParameters
import com.example.chenweiming.mykktapplication.worker.MyWroker

class DaggerWorkerFactory(private val pref: SharedPreferences) : WorkerFactory() {

    override fun createWorker(appContext: Context, workerClassName: String, workerParameters: WorkerParameters): ListenableWorker? {

        val workerKlass = Class.forName(workerClassName).asSubclass(Worker::class.java)
        val constructor = workerKlass.getDeclaredConstructor(Context::class.java, WorkerParameters::class.java)
        val instance = constructor.newInstance(appContext, workerParameters)

        when (instance) {
            is MyWroker -> {
                instance.pref = pref
            }
            // optionally, handle other workers
        }

        return instance
    }
}