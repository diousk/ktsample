package com.example.chenweiming.mykktapplication.di

import android.content.Context
import android.net.ConnectivityManager
import com.example.chenweiming.mykktapplication.api.WikiApiService
import com.example.chenweiming.mykktapplication.repository.*
import com.example.chenweiming.mykktapplication.utils.logger.Logger
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by david.chen@soocii.me on 2018/11/16.
 */
@Module
class RepoModule {
    @Provides
    @Singleton
    fun provideWikiQueryStore(moshi: Moshi,apiService: WikiApiService): WikiQueryStore {
        return WikiQueryRepo(moshi, apiService)
    }

    @Provides
    @Singleton
    fun providePageStore(moshi: Moshi, apiService: WikiApiService, context: Context, logger: Logger): PageStore {
        return PageRepo(moshi, context.cacheDir, apiService, logger)
    }

    @Provides
    @Singleton
    fun providePageStore2(moshi: Moshi, apiService: WikiApiService, context: Context, logger: Logger): PageStore2 {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return PageRepo2(moshi, context, apiService, cm, logger)
    }
}