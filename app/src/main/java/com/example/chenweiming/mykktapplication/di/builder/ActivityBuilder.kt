package com.example.chenweiming.mykktapplication.di.builder

import com.example.chenweiming.mykktapplication.main.MainActivity
import com.example.chenweiming.mykktapplication.main.injection.MainModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by david.chen@soocii.me on 2018/11/8.
 */
@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector(modules = [MainModule::class])
    abstract fun bindMainActivity(): MainActivity
}