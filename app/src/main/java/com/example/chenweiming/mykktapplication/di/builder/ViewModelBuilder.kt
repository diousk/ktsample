package com.example.chenweiming.mykktapplication.di.builder

import android.arch.lifecycle.ViewModelProvider
import com.example.chenweiming.mykktapplication.utils.injection.DaggerAwareViewModelFactory
import dagger.Binds
import dagger.Module

/**
 * Created by david.chen@soocii.me on 2018/11/8.
 */
@Module
internal abstract class ViewModelBuilder {

    @Binds
    internal abstract fun bindViewModelFactory(factory: DaggerAwareViewModelFactory):
            ViewModelProvider.Factory
}