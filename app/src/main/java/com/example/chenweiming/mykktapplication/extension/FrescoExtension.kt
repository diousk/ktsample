@file:JvmName("FrescoUtils")
package com.example.chenweiming.mykktapplication.extension

import android.net.Uri
import android.support.graphics.drawable.VectorDrawableCompat
import android.support.v4.content.ContextCompat
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.drawee.drawable.ScalingUtils
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder
import com.facebook.drawee.generic.RoundingParams
import com.facebook.drawee.view.SimpleDraweeView
import com.facebook.imagepipeline.common.ResizeOptions
import com.facebook.imagepipeline.request.ImageRequest
import com.facebook.imagepipeline.request.ImageRequestBuilder

/**
 * Loads an image from url.
 * @param imgUrl            Image url.
 * @param defaultImgResId   Default image resource id.
 * @param isRound           Is image round?
 * @param backgroundColorId Background color resource id.
 * @param scaleType         Scale type.
 */
fun SimpleDraweeView.loadImgUrl(imgUrl: String?,
                                defaultImgResId: Int,
                                isRound: Boolean = false,
                                backgroundColorId: Int = android.R.color.white,
                                scaleType: ScalingUtils.ScaleType = ScalingUtils.ScaleType.CENTER_CROP) {

    hierarchy = GenericDraweeHierarchyBuilder(context.resources)
        .setPlaceholderImage(defaultImgResId)
        .setPlaceholderImageScaleType(scaleType)
        .setFailureImage(defaultImgResId)
        .setFailureImageScaleType(scaleType)
        .setRoundingParams(RoundingParams().apply {
            roundAsCircle = isRound
            roundingMethod = RoundingParams.RoundingMethod.BITMAP_ONLY
        })
        .setBackground(ContextCompat.getDrawable(context, backgroundColorId))
        .build()

    val request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(imgUrl))
        .setResizeOptions(ResizeOptions.forSquareSize(100))
        .build()

    // Load image url.
    controller = Fresco.newDraweeControllerBuilder().apply{
        callerContext = context
        oldController = controller
        imageRequest = request
    }.build()
}

/**
 * Changes Simple Drawee View Rounding params.
 * @param imgResId        Image resource id.
 * @param isRound           Is image round?
 * @param backgroundColorId Background color resource id.
 * @param scaleType         Scale type.
 */
fun SimpleDraweeView.loadDrawable(imgResId: Int,
                                  isRound: Boolean = false,
                                  backgroundColorId: Int = android.R.color.white,
                                  scaleType: ScalingUtils.ScaleType = ScalingUtils.ScaleType.CENTER_CROP) {

    hierarchy = GenericDraweeHierarchyBuilder(context.resources)
        .setPlaceholderImage(imgResId)
        .setPlaceholderImageScaleType(scaleType)
        .setRoundingParams(RoundingParams().apply {
            roundAsCircle = isRound
            roundingMethod = RoundingParams.RoundingMethod.BITMAP_ONLY
        })
        .setBackground(ContextCompat.getDrawable(context, backgroundColorId))
        .build()

    // Reset controller to avoid old images.
    controller = Fresco.newDraweeControllerBuilder().apply{
        callerContext = context
        oldController = controller
    }.build()
}


/**
 * Loads an image from url.
 * @param imgUrl            Image url.
 * @param defaultImgResId   Default image resource id.
 * @param isRound           Is image round?
 * @param backgroundColorId Background color resource id.
 * @param scaleType         Scale type.
 */
fun SimpleDraweeView.loadImgUrlOrVector(imgUrl: String?,
                                        defaultImgResId: Int,
                                        isRound: Boolean = false,
                                        backgroundColorId: Int = android.R.color.white,
                                        scaleType: ScalingUtils.ScaleType = ScalingUtils.ScaleType.CENTER_CROP) {

    val drawable = VectorDrawableCompat.create(resources, defaultImgResId, context.theme)

    hierarchy = GenericDraweeHierarchyBuilder(context.resources)
        .setPlaceholderImage(drawable)
        .setPlaceholderImageScaleType(scaleType)
        .setFailureImage(drawable)
        .setFailureImageScaleType(scaleType)
        .setRoundingParams(RoundingParams().apply {
            roundAsCircle = isRound
            roundingMethod = RoundingParams.RoundingMethod.BITMAP_ONLY
        })
        .setBackground(ContextCompat.getDrawable(context, backgroundColorId))
        .build()

    // Load image url.
    controller = Fresco.newDraweeControllerBuilder().apply{
        callerContext = context
        oldController = controller
        imgUrl?.let { setUri(Uri.parse(it)) }
    }.build()
}

/**
 * Changes Simple Drawee View Rounding params.
 * @param imgResId        Image resource id.
 * @param isRound           Is image round?
 * @param backgroundColorId Background color resource id.
 * @param scaleType         Scale type.
 */
fun SimpleDraweeView.loadVectorDrawable(imgResId: Int,
                                        isRound: Boolean = false,
                                        backgroundColorId: Int = android.R.color.white,
                                        scaleType: ScalingUtils.ScaleType = ScalingUtils.ScaleType.CENTER_CROP) {

    val drawable = VectorDrawableCompat.create(resources, imgResId, context.theme)

    hierarchy = GenericDraweeHierarchyBuilder(context.resources)
        .setPlaceholderImage(drawable)
        .setPlaceholderImageScaleType(scaleType)
        .setRoundingParams(RoundingParams().apply {
            roundAsCircle = isRound
            roundingMethod = RoundingParams.RoundingMethod.BITMAP_ONLY
        })
        .setBackground(ContextCompat.getDrawable(context, backgroundColorId))
        .build()

    // Reset controller to avoid old images.
    controller = Fresco.newDraweeControllerBuilder().apply{
        callerContext = context
        oldController = controller
    }.build()
}