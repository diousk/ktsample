package com.example.chenweiming.mykktapplication.initializers

import android.app.Application

/**
 * Created by david.chen@soocii.me on 2018/11/8.
 */
interface AppInitializer {
    fun init(application: Application)
}