package com.example.chenweiming.mykktapplication.initializers

import android.app.ActivityManager
import android.app.Application
import android.content.ComponentCallbacks2
import android.content.Context
import android.content.res.Configuration
import android.graphics.Bitmap
import android.util.Log
import com.example.chenweiming.mykktapplication.utils.ByteConst
import com.facebook.cache.disk.DiskCacheConfig
import com.facebook.common.disk.NoOpDiskTrimmableRegistry
import com.facebook.common.internal.Supplier
import com.facebook.common.memory.MemoryTrimType
import com.facebook.common.memory.MemoryTrimmable
import com.facebook.common.memory.MemoryTrimmableRegistry
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.backends.okhttp3.OkHttpImagePipelineConfigFactory
import com.facebook.imagepipeline.cache.MemoryCacheParams
import okhttp3.OkHttpClient
import javax.inject.Inject
import javax.inject.Named
import com.facebook.common.memory.NoOpMemoryTrimmableRegistry
import com.facebook.imagepipeline.core.ImagePipelineFactory

class FrescoInitializer
@Inject constructor(
    @Named("image") private val okHttpClient: OkHttpClient
) : AppInitializer {

    override fun init(application: Application) {
        // setup low memory listener
        application.registerComponentCallbacks(componentCallback())

        val diskCacheConfig = DiskCacheConfig.newBuilder(application)
            .setMaxCacheSize(100 * ByteConst.MB.toLong())
            .setMaxCacheSizeOnLowDiskSpace(60 * ByteConst.MB.toLong())
            .setMaxCacheSizeOnVeryLowDiskSpace(20 * ByteConst.MB.toLong())
            .setDiskTrimmableRegistry(NoOpDiskTrimmableRegistry.getInstance())
            .build()
        val smallImageDiskCacheConfig = DiskCacheConfig.newBuilder(application)
            .setMaxCacheSize((20 * ByteConst.MB).toLong())
            .setMaxCacheSizeOnLowDiskSpace((12 * ByteConst.MB).toLong())
            .setMaxCacheSizeOnVeryLowDiskSpace((4 * ByteConst.MB).toLong())
            .setDiskTrimmableRegistry(NoOpDiskTrimmableRegistry.getInstance())
            .build()

        val trimRegistry = memoryTrimRegistry()

        val config = OkHttpImagePipelineConfigFactory.newBuilder(application, okHttpClient)
            .setDownsampleEnabled(true)
            .setResizeAndRotateEnabledForNetwork(true)
            .setBitmapsConfig(Bitmap.Config.RGB_565)
            .setMainDiskCacheConfig(diskCacheConfig)
            .setSmallImageDiskCacheConfig(smallImageDiskCacheConfig)
            .setBitmapMemoryCacheParamsSupplier(FrescoMemoryCacheSupplier(application.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager))
            .setMemoryTrimmableRegistry(trimRegistry)
            .build()
        Fresco.initialize(application, config)
    }

    private fun memoryTrimRegistry(): MemoryTrimmableRegistry {
        val memoryTrimmableRegistry = NoOpMemoryTrimmableRegistry.getInstance()
        memoryTrimmableRegistry.registerMemoryTrimmable { trimType ->
            val suggestedTrimRatio = trimType.suggestedTrimRatio
            when (suggestedTrimRatio) {
                MemoryTrimType.OnCloseToDalvikHeapLimit.suggestedTrimRatio,
                MemoryTrimType.OnSystemLowMemoryWhileAppInBackground.suggestedTrimRatio,
                MemoryTrimType.OnSystemLowMemoryWhileAppInForeground.suggestedTrimRatio ->
                    clearMemoryCaches()
            }
        }
        return memoryTrimmableRegistry
    }

    private fun componentCallback(): ComponentCallbacks2 {
        return object : ComponentCallbacks2{
            override fun onLowMemory() {
                clearMemoryCaches()
            }

            override fun onConfigurationChanged(newConfig: Configuration?) {
            }

            override fun onTrimMemory(level: Int) {
                clearMemoryCaches()
            }
        }
    }

    private fun clearMemoryCaches() {
        try {
            ImagePipelineFactory.getInstance().imagePipeline.clearMemoryCaches()
        } catch (e: Exception) {}
    }
}

class FrescoMemoryCacheSupplier(private val activityManager: ActivityManager) : Supplier<MemoryCacheParams> {
    private val maxCacheSize: Int
        get() {
            val maxMemory = Math.min(activityManager.memoryClass * ByteConst.MB, Integer.MAX_VALUE)
            return when {
                maxMemory < 32 * ByteConst.MB -> 4 * ByteConst.MB
                maxMemory < 64 * ByteConst.MB -> 6 * ByteConst.MB
                else -> 16 * ByteConst.MB
            }
        }

    override fun get(): MemoryCacheParams {
        return MemoryCacheParams(
            maxCacheSize,
            MAX_CACHE_ENTRIES,
            maxCacheSize,
            MAX_CACHE_EVICTION_ENTRIES,
            maxCacheSize
        )
    }

    companion object {
        private const val MAX_CACHE_ENTRIES = 64
        private const val MAX_CACHE_EVICTION_ENTRIES = 10
    }
}