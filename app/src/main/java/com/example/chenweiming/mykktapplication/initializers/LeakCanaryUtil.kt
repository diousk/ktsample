package com.example.chenweiming.mykktapplication.initializers

import android.app.Application
import com.squareup.leakcanary.LeakCanary
import com.squareup.leakcanary.RefWatcher
import javax.inject.Inject

class LeakCanaryUtil @Inject constructor(): AppInitializer {
    override fun init(application: Application) {
        if (LeakCanary.isInAnalyzerProcess(application)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return
        }
        sRefWatcher = LeakCanary.install(application)
    }

    companion object {
        var sRefWatcher: RefWatcher? = null
        fun watch(obj: Any) {
            sRefWatcher?.watch(obj)
        }
    }
}