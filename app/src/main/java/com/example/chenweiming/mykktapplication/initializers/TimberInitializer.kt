package com.example.chenweiming.mykktapplication.initializers

import android.app.Application
import android.content.SharedPreferences
import com.example.chenweiming.mykktapplication.BuildConfig
import com.example.chenweiming.mykktapplication.utils.logger.TimberLogger
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by david.chen@soocii.me on 2018/11/8.
 */
class TimberInitializer
@Inject constructor(private val timberLogger: TimberLogger,
                    @Named("app")
                    private val appPreference: SharedPreferences)
    : AppInitializer {
    override fun init(application: Application) {
        val debuggable = appPreference.getBoolean("debuggable", false)
        timberLogger.setup(BuildConfig.DEBUG || debuggable)
    }
}