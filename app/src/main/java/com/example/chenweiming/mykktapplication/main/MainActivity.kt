package com.example.chenweiming.mykktapplication.main

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.SharedPreferences
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.widget.Toast
import androidx.work.*
import com.example.chenweiming.mykktapplication.R
import com.example.chenweiming.mykktapplication.api.response.PageResponse
import com.example.chenweiming.mykktapplication.const.PrefConst
import com.example.chenweiming.mykktapplication.databinding.ActivityMainBinding
import com.example.chenweiming.mykktapplication.extension.PreferenceExtension.get
import com.example.chenweiming.mykktapplication.extension.PreferenceExtension.set
import com.example.chenweiming.mykktapplication.main.view.MainView
import com.example.chenweiming.mykktapplication.main.viewmodel.MainViewModel
import com.example.chenweiming.mykktapplication.repository.UserMe
import com.example.chenweiming.mykktapplication.utils.logger.Logger
import com.example.chenweiming.mykktapplication.utils.view.PreCachingLayoutManager
import com.example.chenweiming.mykktapplication.worker.MyWroker
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Named

class MainActivity : DaggerAppCompatActivity(), MainView {
    @Inject lateinit var logger: Logger

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: ActivityMainBinding

    @Inject @field:Named(PrefConst.App.PREF_NAME)
    lateinit var appPref: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        binding.viewModel = viewModel

        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        rvPages.adapter = PageAdapter(viewModel)
        rvPages.layoutManager = PreCachingLayoutManager(this)
    }

    override fun showToast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }

    override fun updateText(text: String, debuggable: Boolean) {
        val prevDebuggable = appPref[PrefConst.App.DEBUGGABLE, false]
        logger.d("pre: debuggable: $prevDebuggable")
        appPref[PrefConst.App.DEBUGGABLE] = debuggable
        logger.d("cur: debuggable: $debuggable")
        logger.d("cur: user name: ${UserMe.name}")
        UserMe.update(text)
        logger.d("after: user name: ${UserMe.name}")

        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        // Add WorkRequest to save the image to the filesystem
        val save = OneTimeWorkRequest.Builder(MyWroker::class.java)
            .setConstraints(constraints)
            .setInitialDelay(5, TimeUnit.SECONDS)
            .setInputData(createInputDataForUri())
            .addTag("HELLO")
            .build()

        WorkManager.getInstance()
            .enqueueUniqueWork("WW", ExistingWorkPolicy.APPEND, save)

        textView.text = text
    }

    private var count = 0

    private fun createInputDataForUri(): Data {
        count++
        val builder = Data.Builder()
        builder.putString("key1", "100")
        builder.putInt("key2", 20000 + count)
        return builder.build()
    }

    override fun updateRecords(pages: List<PageResponse.Page>) {
        logger.d("updateRecords - pages: %s", pages)
        (rvPages.adapter as PageAdapter).update(pages)
    }
}
