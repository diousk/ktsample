package com.example.chenweiming.mykktapplication.main

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.chenweiming.mykktapplication.api.response.PageResponse
import com.example.chenweiming.mykktapplication.databinding.PageItemBinding
import com.example.chenweiming.mykktapplication.main.viewmodel.MainViewModel

class PageAdapter(
    private val viewModel: MainViewModel
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var items: List<PageResponse.Page> = emptyList()

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PageViewHolder(PageItemBinding.inflate(LayoutInflater.from(parent.context), parent, false), viewModel)
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (viewHolder is PageViewHolder) {
            val item = items[position]
            viewHolder.item = item
            viewHolder.bindView(item)
        }
    }

    fun update(pageList: List<PageResponse.Page>) {
        items = pageList
        notifyDataSetChanged()
    }

    override fun getItemCount() = items.size

    override fun getItemId(position: Int): Long {
        return items[position].pageid.toLong()
    }
}