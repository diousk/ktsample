package com.example.chenweiming.mykktapplication.main

import android.support.v7.widget.RecyclerView
import com.example.chenweiming.mykktapplication.R
import com.example.chenweiming.mykktapplication.api.response.PageResponse
import com.example.chenweiming.mykktapplication.databinding.PageItemBinding
import com.example.chenweiming.mykktapplication.extension.loadImgUrl
import com.example.chenweiming.mykktapplication.main.viewmodel.MainViewModel
import timber.log.Timber

class PageViewHolder(
    private val binding: PageItemBinding,
    private val viewModel: MainViewModel
) : RecyclerView.ViewHolder(binding.root){
    lateinit var item: PageResponse.Page
    init {
        binding.photo.setOnClickListener { viewModel.onPageClick(item) }
    }

    fun bindView(page: PageResponse.Page) {
        binding.title.text = page.title
        binding.photoDescription.text = page.pageimage
        binding.photo.loadImgUrl(page.thumbnail?.source ?: "", R.mipmap.ic_launcher)
        binding.executePendingBindings()
    }
}