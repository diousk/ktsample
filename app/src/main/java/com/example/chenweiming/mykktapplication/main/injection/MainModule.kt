package com.example.chenweiming.mykktapplication.main.injection

import android.arch.lifecycle.ViewModel
import android.content.SharedPreferences
import com.example.chenweiming.mykktapplication.main.MainActivity
import com.example.chenweiming.mykktapplication.main.view.MainView
import com.example.chenweiming.mykktapplication.main.viewmodel.MainViewModel
import com.example.chenweiming.mykktapplication.main.model.MainModel
import com.example.chenweiming.mykktapplication.main.model.MainModelImpl
import com.example.chenweiming.mykktapplication.repository.PageStore
import com.example.chenweiming.mykktapplication.repository.PageStore2
import com.example.chenweiming.mykktapplication.repository.WikiQueryStore
import com.example.chenweiming.mykktapplication.utils.injection.ViewModelKey
import com.example.chenweiming.mykktapplication.utils.logger.Logger
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import javax.inject.Named

/**
 * Created by david.chen@soocii.me on 2018/11/8.
 */
@Module
class MainModule {
    @Provides
    fun provideMainModel(wikiQueryStore: WikiQueryStore, pageStore: PageStore2, @Named("app") preference: SharedPreferences): MainModel {
        return MainModelImpl(wikiQueryStore, pageStore, preference)
    }

    @Provides
    fun provideMainView(mainActivity: MainActivity): MainView {
        return mainActivity
    }

    @Provides
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun provideMainViewModel(logger: Logger, mainModel: MainModel, mainView: MainView): ViewModel {
        return MainViewModel(logger, mainModel, mainView)
    }
}