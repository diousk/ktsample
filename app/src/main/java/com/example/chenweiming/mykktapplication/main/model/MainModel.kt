package com.example.chenweiming.mykktapplication.main.model

import com.example.chenweiming.mykktapplication.api.response.PageResponse
import com.example.chenweiming.mykktapplication.api.response.SearchResponse
import io.reactivex.Single

/**
 * Created by david.chen@soocii.me on 2018/11/9.
 */
interface MainModel {
    fun isDebuggable(): Boolean
    fun setDebuggable(debuggable: Boolean)

    fun queryKeyword(query: String): Single<List<SearchResponse.Record>>
    fun queryPage(pageId: Int): Single<PageResponse.Page>

    fun getPage(pageId: Int): Single<PageResponse.Page>

    fun clear()
}