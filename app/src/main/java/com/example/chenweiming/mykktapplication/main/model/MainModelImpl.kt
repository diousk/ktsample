package com.example.chenweiming.mykktapplication.main.model

import android.content.SharedPreferences
import com.example.chenweiming.mykktapplication.api.response.PageResponse
import com.example.chenweiming.mykktapplication.api.response.SearchResponse
import com.example.chenweiming.mykktapplication.repository.PageStore
import com.example.chenweiming.mykktapplication.repository.PageStore2
import com.example.chenweiming.mykktapplication.repository.WikiQueryStore
import io.reactivex.Single

/**
 * Created by david.chen@soocii.me on 2018/11/15.
 */
class MainModelImpl(
    private val wikiQueryStore: WikiQueryStore,
    private val pageStore: PageStore2,
    private val appPreference: SharedPreferences
): MainModel {

    override fun isDebuggable(): Boolean {
        return appPreference.getBoolean("debuggable", false)
    }

    override fun setDebuggable(debuggable: Boolean) {
        appPreference.edit().putBoolean("debuggable", debuggable).apply()
    }

    override fun queryKeyword(query: String): Single<List<SearchResponse.Record>> {
        return wikiQueryStore.query(query)
    }

    override fun queryPage(pageId: Int): Single<PageResponse.Page> {
        return pageStore.get(pageId)
            .map { it.query.pages[pageId.toString()]!! }
            .toSingle()
    }

    override fun getPage(pageId: Int): Single<PageResponse.Page> {
        return pageStore.get(pageId)
            .map { it.query.pages[pageId.toString()]!! }
            .toSingle()
    }

    override fun clear() {
        wikiQueryStore.clearAll()
        pageStore.clear()
    }
}