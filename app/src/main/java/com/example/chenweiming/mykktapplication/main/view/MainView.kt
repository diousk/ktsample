package com.example.chenweiming.mykktapplication.main.view

import android.arch.lifecycle.LifecycleOwner
import com.example.chenweiming.mykktapplication.api.response.PageResponse

interface MainView: LifecycleOwner{
    fun updateText(text: String, debuggable: Boolean)
    fun updateRecords(pages: List<PageResponse.Page>)
    fun showToast(text: String)
}