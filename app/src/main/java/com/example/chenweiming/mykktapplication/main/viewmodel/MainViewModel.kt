package com.example.chenweiming.mykktapplication.main.viewmodel

import com.example.chenweiming.mykktapplication.api.response.PageResponse
import com.example.chenweiming.mykktapplication.extension.plusAssign
import com.example.chenweiming.mykktapplication.main.model.MainModel
import com.example.chenweiming.mykktapplication.main.view.MainView
import com.example.chenweiming.mykktapplication.utils.AudoDisposeViewModel
import com.example.chenweiming.mykktapplication.utils.logger.Logger
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * Created by david.chen@soocii.me on 2018/11/8.
 */
class MainViewModel(
    val logger: Logger, val mainModel: MainModel, val mainView: MainView
): AudoDisposeViewModel() {

    fun doFetch() {
//        compositeDisposable += mainModel.queryKeyword("Trump")
//            .flatMapObservable { Observable.fromIterable(it) }
//            .flatMap { mainModel.getPage(it.pageid).toObservable() }
//            .doOnNext { logger.d("page: %s", it) }
//            .toSortedList()
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe { result ->
//                mainView.updateRecords(result)
//            }
        compositeDisposable += mainModel.getPage(6191053)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe ({ result ->
                logger.d("6191053: '%s'", result.toString())
            }, {
                logger.d("error: $it")
            })
    }

    fun toggleDebuggable(type: Int) {
        mainModel.setDebuggable(!mainModel.isDebuggable())
        mainView.updateText("app debuggable = ${mainModel.isDebuggable()}", mainModel.isDebuggable())
    }

    fun onPageClick(page: PageResponse.Page) {
        mainView.showToast("page ${page.title} clicked!")
    }

    override fun onCleared() {
        super.onCleared()
        logger.d("onCleared")
        mainModel.clear()
    }
}