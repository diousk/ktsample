package com.example.chenweiming.mykktapplication.repository

import com.example.chenweiming.mykktapplication.BuildConfig
import com.example.chenweiming.mykktapplication.api.WikiApiService
import com.example.chenweiming.mykktapplication.api.response.PageResponse
import com.example.chenweiming.mykktapplication.repository.base.CacheStore
import com.example.chenweiming.mykktapplication.repository.base.CacheStoreT
import com.example.chenweiming.mykktapplication.utils.ByteConst
import com.example.chenweiming.mykktapplication.utils.cache.DualCachePersister
import com.example.chenweiming.mykktapplication.utils.cache.JsonSerializer
import com.example.chenweiming.mykktapplication.utils.logger.Logger
import com.nytimes.android.external.store3.base.Fetcher
import com.nytimes.android.external.store3.base.impl.*
import com.nytimes.android.external.store3.middleware.moshi.MoshiParserFactory
import com.squareup.moshi.Moshi
import com.vincentbrison.openlibraries.android.dualcache.Builder
import com.vincentbrison.openlibraries.android.dualcache.DualCache
import io.reactivex.Maybe
import io.reactivex.Single
import okio.BufferedSource
import java.io.File

interface PageStore: CacheStoreT<Int, PageResponse.Page> {
    fun query(pageId: Int): Single<PageResponse.Page>
}

class PageRepo(moshi: Moshi, cacheDir: File,
    private val wikiApiService: WikiApiService,
    private val logger: Logger
): PageStore {
    companion object {
        const val VERSION_CODE = BuildConfig.VERSION_CODE + CacheStore.VERSION
        const val CACHE_ID = "page_cache"
        const val CACHE_SIZE_BYTES = 10 * ByteConst.KB
    }

    private val jsonSerializer = JsonSerializer(PageResponse.Result::class.java, moshi)
    private val cache: DualCache<PageResponse.Result>
    private val store: Store<PageResponse.Result, Int>

    init {
        // init cache
        cache = Builder<PageResponse.Result>(CACHE_ID, VERSION_CODE)
            .useSerializerInDisk(CACHE_SIZE_BYTES, cacheDir, jsonSerializer)
            .noRam()
            .enableLog()
            .build()

        // init store - fetcher
        val rawFetcher = Fetcher<BufferedSource, Int> {
            wikiApiService.getPageRaw(it)
                .map { raw -> raw.source() }
        }

        // init store - parser
        val parser = MoshiParserFactory.createSourceParser<PageResponse.Result>(moshi, PageResponse.Result::class.java)

        // init store
        store = FluentStoreBuilder.key(fetcher = ParsingFetcher.from(rawFetcher, parser)) {
            persister = DualCachePersister<PageResponse.Result, Int>(cache, logger)
            memoryPolicy = MemoryPolicy.builder().setExpireAfterWrite(5).setMemorySize(1).build()
        }
    }

    override fun query(pageId: Int): Single<PageResponse.Page> {
        return wikiApiService.getPage(pageId)
            .map { result -> result.query.pages[pageId.toString()] }
    }

    override fun get(key: Int): Maybe<PageResponse.Page> {
        return store.get(key)
            .map { it.query.pages[key.toString()]!! }
            .toMaybe()
    }

    override fun fetch(key: Int): Maybe<PageResponse.Page> {
        return store.fetch(key)
            .map { it.query.pages[key.toString()]!! }
            .toMaybe()
    }

    override fun clear(key: Int) {
        cache.delete(key.toString())
        store.clear(key)
    }

    override fun clearAll() {
        cache.invalidate()
        store.clear()
    }
}