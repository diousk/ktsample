package com.example.chenweiming.mykktapplication.repository

import android.content.Context
import android.net.ConnectivityManager
import com.example.chenweiming.mykktapplication.api.WikiApiService
import com.example.chenweiming.mykktapplication.api.response.PageResponse
import com.example.chenweiming.mykktapplication.repository.base.CacheRepository
import com.example.chenweiming.mykktapplication.utils.ByteConst
import com.example.chenweiming.mykktapplication.utils.cache.JsonSerializer
import com.example.chenweiming.mykktapplication.utils.logger.Logger
import com.nytimes.android.external.store3.base.Fetcher
import com.nytimes.android.external.store3.middleware.moshi.MoshiParserFactory
import com.squareup.moshi.Moshi
import io.reactivex.Maybe
import io.reactivex.internal.operators.maybe.MaybeJust
import okio.BufferedSource
import java.lang.RuntimeException
import java.util.concurrent.TimeUnit

interface PageStore2 {
    fun get(key: Int) : Maybe<PageResponse.Result>
    fun update(key: Int, value: PageResponse.Result)
    fun clearKey(key: Int)
    fun clear()
}

class PageRepo2(moshi: Moshi, context: Context,
                wikiApiService: WikiApiService,
                private val cm: ConnectivityManager,
                private val logger: Logger
): PageStore2 {
    private var cacheRepository: CacheRepository<Int, PageResponse.Result>

    init {
        val jsonSerializer = JsonSerializer(PageResponse.Result::class.java, moshi)
        val networkFetcher = Fetcher<BufferedSource, Int> {
            wikiApiService.getPageRaw(it).map { raw -> raw.source() }
        }
        val networkParser = MoshiParserFactory.createSourceParser<PageResponse.Result>(moshi, PageResponse.Result::class.java)
        cacheRepository = CacheRepository.RepoBuilder<Int, PageResponse.Result>()
            .cacheId("page_repo2")
            .jsonSerializer(jsonSerializer)
            .useDiskCache(context, false, 100*ByteConst.KB)
            .useMemoryCache(1, 100*ByteConst.KB)
            .useNetwork(networkFetcher, networkParser)
            .expirationTimeout(TimeUnit.SECONDS.toMillis(20))
            .logger(logger)
            .build()
    }

    override fun update(key: Int, value: PageResponse.Result) {
        cacheRepository.put(key, value)
    }

    override fun get(key: Int): Maybe<PageResponse.Result> {
        val connected = cm.activeNetworkInfo?.isConnected ?: false
        logger.d("get id : $key, connected $connected")
        return if (connected) {
            cacheRepository.get(key).toMaybe()

        } else {
            Maybe.just(cacheRepository.local(key))
        }
    }

    override fun clearKey(key: Int) {
        logger.d("clearKey $key")
        cacheRepository.clear(key)
    }

    override fun clear() {
        logger.d("clear all")
        cacheRepository.clearAll()
    }
}