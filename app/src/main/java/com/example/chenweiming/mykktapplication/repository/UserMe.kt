package com.example.chenweiming.mykktapplication.repository

import com.example.chenweiming.mykktapplication.App
import com.example.chenweiming.mykktapplication.extension.PreferenceExtension
import com.example.chenweiming.mykktapplication.extension.PreferenceExtension.get
import com.example.chenweiming.mykktapplication.extension.PreferenceExtension.set

// not testable object
object UserMe {
    var name: String = ""
        get() {return if (field.isEmpty()) {
            field = PreferenceExtension.appPrefs(App.instance)["auth.name", "unknown"]!!
            field}
        else {field}}
        set(value) { PreferenceExtension.appPrefs(App.instance)["auth.name"] = value
            field = value}

    fun update(name: String) = run { this.name = name }
}