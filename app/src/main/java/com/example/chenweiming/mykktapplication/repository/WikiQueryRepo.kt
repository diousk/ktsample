package com.example.chenweiming.mykktapplication.repository

import com.example.chenweiming.mykktapplication.BuildConfig
import com.example.chenweiming.mykktapplication.api.WikiApiService
import com.example.chenweiming.mykktapplication.api.response.SearchResponse
import com.example.chenweiming.mykktapplication.repository.base.CacheStore
import com.example.chenweiming.mykktapplication.repository.base.CacheStoreT
import com.example.chenweiming.mykktapplication.utils.cache.JsonSerializer
import com.nytimes.android.external.store3.base.Fetcher
import com.nytimes.android.external.store3.base.Persister
import com.nytimes.android.external.store3.base.impl.ParsingFetcher
import com.nytimes.android.external.store3.base.impl.Store
import com.nytimes.android.external.store3.base.impl.StoreBuilder
import com.nytimes.android.external.store3.middleware.moshi.MoshiParserFactory
import com.squareup.moshi.Moshi
import com.vincentbrison.openlibraries.android.dualcache.Builder
import com.vincentbrison.openlibraries.android.dualcache.DualCache
import io.reactivex.Maybe
import io.reactivex.Single
import okio.BufferedSource
import java.lang.Exception

/**
 * Created by david.chen@soocii.me on 2018/11/16.
 */
interface WikiQueryStore: CacheStoreT<String, SearchResponse.Result> {
    fun query(query: String): Single<List<SearchResponse.Record>>
}

class WikiQueryRepo(moshi: Moshi, private val wikiApiService: WikiApiService): WikiQueryStore {
    companion object {
        const val VERSION_CODE = BuildConfig.VERSION_CODE + 1
        const val CACHE_ID = "record_cache"
        const val MEMORY_CACHE_SIZE_BYTES = 1024 * 50
    }

    private val jsonSerializer =
        JsonSerializer(SearchResponse.Result::class.java, moshi)
    private val cache: DualCache<SearchResponse.Result>
    private val store: Store<SearchResponse.Result, String>

    init {
        // init cache
        cache = Builder<SearchResponse.Result>(CACHE_ID, VERSION_CODE)
            .useSerializerInRam(MEMORY_CACHE_SIZE_BYTES, jsonSerializer)
            .noDisk()
            .build()

        // init store
        val rawFetcher = Fetcher<BufferedSource, String> {
            wikiApiService.hitCountCheckRaw(it)
                .map { raw -> raw.source() }
        }

        store = StoreBuilder.key<String, SearchResponse.Result>()
            .fetcher(ParsingFetcher.from(rawFetcher, MoshiParserFactory.createSourceParser(moshi, SearchResponse.Result::class.java)))
            .persister(object : Persister<SearchResponse.Result, String> {
                override fun write(key: String, record: SearchResponse.Result): Single<Boolean> {
                    return try {
                        cache.put(key, record)
                        Single.just(true)
                    } catch (e: Exception) {
                        Single.just(false)
                    }
                }

                override fun read(key: String): Maybe<SearchResponse.Result> {
                    return try {
                        Maybe.just(cache.get(key))
                    } catch (e: Exception) {
                        Maybe.empty<SearchResponse.Result>()
                    }
                }
            })
            .open()
    }

    override fun query(query: String): Single<List<SearchResponse.Record>> {
        // TODO: use local cache first
        return wikiApiService.hitCountCheck(query)
            .map{result -> result.query.search}
    }

    override fun get(key: String): Maybe<SearchResponse.Result> {
        return store.get(key).toMaybe()
    }

    override fun fetch(key: String): Maybe<SearchResponse.Result> {
        return store.fetch(key).toMaybe() //To change body of created functions use File | Settings | File Templates.
    }

    override fun clear(key: String) {
        cache.delete(key)
        store.clear(key)
    }

    override fun clearAll() {
        cache.invalidate()
        store.clear()
    }
}