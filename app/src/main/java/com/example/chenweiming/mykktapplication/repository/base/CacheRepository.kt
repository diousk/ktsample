package com.example.chenweiming.mykktapplication.repository.base

import android.content.Context
import com.example.chenweiming.mykktapplication.BuildConfig
import com.example.chenweiming.mykktapplication.utils.ByteConst
import com.example.chenweiming.mykktapplication.utils.cache.DualCachePersister
import com.example.chenweiming.mykktapplication.utils.cache.JsonSerializer
import com.example.chenweiming.mykktapplication.utils.logger.Logger
import com.nytimes.android.external.store3.base.Fetcher
import com.nytimes.android.external.store3.base.Parser
import com.nytimes.android.external.store3.base.impl.FluentStoreBuilder
import com.nytimes.android.external.store3.base.impl.MemoryPolicy
import com.nytimes.android.external.store3.base.impl.ParsingFetcher
import com.nytimes.android.external.store3.base.impl.Store
import com.vincentbrison.openlibraries.android.dualcache.Builder
import com.vincentbrison.openlibraries.android.dualcache.DualCache
import io.reactivex.Single
import okio.BufferedSource
import java.lang.Exception
import java.util.concurrent.TimeUnit

class CacheRepository<Key, Type>(
    cacheId: String,
    jsonSerializer: JsonSerializer<Type>,
    useMemoryCache: Boolean = false,
    storeMemoryCacheNumber: Long = 100, // number of items in memory
    memoryCacheSizeBytes: Int = 100 * ByteConst.KB,
    useDiskCache: Boolean = false,
    context: Context? = null,
    usePrivateFile: Boolean = false,
    diskCacheSizeBytes: Int = 100 * ByteConst.KB,
    expirationTimeout: Long = -1L,
    private val useNetwork: Boolean = false,
    networkFetcher: Fetcher<BufferedSource, Key>? = null,
    networkParser: Parser<BufferedSource, Type>? = null,
    private val logger: Logger? = null
) : CacheStore<Key, Type> {

    companion object {
        const val VERSION_CODE = BuildConfig.VERSION_CODE + CacheStore.VERSION
    }

    private var cache: DualCache<Type>
    private var store: Store<Type, Key>? = null
    init {
        // init dual cache
        cache = with(Builder<Type>(cacheId, VERSION_CODE)) {
            if (useDiskCache && context != null) {
                useSerializerInDisk(diskCacheSizeBytes, usePrivateFile, jsonSerializer, context)
            } else {
                noDisk()
            }
            // use memory cache in Store if network enable
            if (useMemoryCache && !useNetwork) {
                useSerializerInRam(memoryCacheSizeBytes, jsonSerializer)
            } else {
                noRam()
            }
            if (logger != null) {
                enableLog()
            }
            build()
        }

        if (useNetwork && networkFetcher != null && networkParser != null) {
            // init store
            val storeMemoryPolicy = with(MemoryPolicy.builder()) {
                if (expirationTimeout == -1L) {
                    // default memory cache expire after 24 hours
                    setExpireAfterWrite(24)
                    setExpireAfterTimeUnit(TimeUnit.HOURS)
                } else {
                    setExpireAfterWrite((expirationTimeout / 2) / 1000)
                }
                setMemorySize(storeMemoryCacheNumber)
                build()
            }
            store = FluentStoreBuilder.key(fetcher = ParsingFetcher.from(networkFetcher, networkParser)) {
                persister = DualCachePersister<Type, Key>(cache, logger, expirationTimeout)
                memoryPolicy = storeMemoryPolicy
            }
        }
    }

    override fun local(key: Key): Type? {
        return cache.get(key.toString())
    }

    override fun get(key: Key): Single<Type> {
        return if (useNetwork) {
            store!!.get(key)
        } else {
            Single.fromCallable { cache.get(key.toString()) }
        }
    }

    override fun fetch(key: Key): Single<Type> {
        return if (useNetwork) {
            store!!.fetch(key)
        } else {
            Single.fromCallable { cache.get(key.toString()) }
        }
    }

    override fun put(key: Key, value: Type) {
        clear(key)
        cache.put(key.toString(), value)
        logger?.d("cache diskBytesUsed: ${cache.diskUsedInBytes}")
    }

    override fun clear(key: Key) {
        try {
            cache.delete(key.toString())
        } catch (ignore: Exception){}
        store?.clear(key)
    }

    override fun clearAll() {
        cache.invalidate()
        store?.clear()
    }

    class RepoBuilder<Key, Type>{
        private lateinit var cacheId: String
        private lateinit var jsonSerializer: JsonSerializer<Type>

        private var useMemoryCache: Boolean = false
        private var storeMemoryCacheNumber: Long = 100 // number of items in memory
        private var memoryCacheSizeBytes: Int = 100 * ByteConst.KB

        private var useDiskCache: Boolean = false
        private var context: Context? = null
        private var usePrivateFile: Boolean = false
        private var diskCacheSizeBytes: Int = 100 * ByteConst.KB
        private var expirationTimeout: Long = -1 // default cache until clear

        private var useNetwork: Boolean = false
        private var networkFetcher: Fetcher<BufferedSource, Key>? = null
        private var networkParser: Parser<BufferedSource, Type>? = null
        private var logger: Logger? = null

        // cache properties
        fun cacheId(cacheId: String) = apply { this.cacheId = cacheId }
        fun jsonSerializer(jsonSerializer: JsonSerializer<Type>) = apply { this.jsonSerializer = jsonSerializer }

        // memory config
        fun useMemoryCache(storeMemoryCacheNumber: Long, memoryCacheSizeBytes: Int) = apply {
            this.useMemoryCache = true
            this.storeMemoryCacheNumber = storeMemoryCacheNumber
            this.memoryCacheSizeBytes = memoryCacheSizeBytes
        }

        // disk config
        fun useDiskCache(context: Context, usePrivateFile: Boolean, diskCacheSizeBytes: Int) = apply {
            this.useDiskCache = true
            this.context = context
            this.usePrivateFile = usePrivateFile
            this.diskCacheSizeBytes = diskCacheSizeBytes
        }

        // cache expiration
        fun expirationTimeout(expirationTimeout: Long) = apply { this.expirationTimeout = expirationTimeout }

        // network config
        fun useNetwork(networkFetcher: Fetcher<BufferedSource, Key>, networkParser: Parser<BufferedSource, Type>) = apply {
            this.useNetwork = true
            this.networkFetcher = networkFetcher
            this.networkParser = networkParser
        }

        fun logger(logger: Logger) = apply { this.logger = logger }

        fun build() = CacheRepository(
            cacheId,
            jsonSerializer,
            useMemoryCache,
            storeMemoryCacheNumber,
            memoryCacheSizeBytes,
            useDiskCache,
            context,
            usePrivateFile,
            diskCacheSizeBytes,
            expirationTimeout,
            useNetwork,
            networkFetcher,
            networkParser,
            logger
        )
    }

}