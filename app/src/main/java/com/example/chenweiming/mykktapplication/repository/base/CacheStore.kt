package com.example.chenweiming.mykktapplication.repository.base

import io.reactivex.Maybe
import io.reactivex.Single

interface CacheStore<K, V> {
    fun local(key: K) : V?
    fun get(key: K) : Single<V>
    fun fetch(key: K) : Single<V>
    fun put(key: K, value: V)
    fun clear(key: K)
    fun clearAll()

    companion object {
        const val VERSION = 1
    }
}

interface CacheStoreT<K, V> {
    fun get(key: K) : Maybe<V>
    fun fetch(key: K) : Maybe<V>
    fun clear(key: K)
    fun clearAll()

    companion object {
        const val VERSION = 2
    }
}