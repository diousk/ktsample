package com.example.chenweiming.mykktapplication.repository.base

interface Timestamped {
     var timestamp: Long
}