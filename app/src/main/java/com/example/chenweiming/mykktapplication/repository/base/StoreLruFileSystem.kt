package com.example.chenweiming.mykktapplication.repository.base

import android.util.Log
import com.jakewharton.disklrucache.DiskLruCache
import com.nytimes.android.external.fs3.filesystem.FileSystem
import com.nytimes.android.external.store3.base.RecordState
import com.vincentbrison.openlibraries.android.dualcache.CacheSerializer
import com.vincentbrison.openlibraries.android.dualcache.DualCache

import java.io.FileNotFoundException
import java.io.IOException
import java.io.OutputStream
import java.util.concurrent.TimeUnit
import javax.inject.Inject

import io.reactivex.exceptions.Exceptions
import okio.BufferedSink
import okio.BufferedSource
import okio.Okio
import java.io.InputStream
import java.nio.charset.StandardCharsets

class StoreLruFileSystem<T>(
    private val lruCache: DualCache<T>,
    private val serializer: CacheSerializer<T>
) : FileSystem {

    @Throws(FileNotFoundException::class)
    override fun read(path: String): BufferedSource {
        Log.d("LRU", "read: $path")
        try {
            val snapshot = lruCache.get(path) ?: throw FileNotFoundException(path)
            return Okio.buffer(Okio.source(serializer.toString(snapshot).byteInputStream(StandardCharsets.UTF_8)))

        } catch (e: IOException) {
            throw Exceptions.propagate(e)
        }

    }

    @Throws(IOException::class)
    override fun write(path: String, source: BufferedSource) {
        val jsonString = source.readUtf8()
        Log.d("LRU", "write: $path, json: $jsonString")
        lruCache.put(path, serializer.fromString(jsonString))
    }

    @Throws(IOException::class)
    override fun delete(path: String) {
        lruCache.delete(path)
    }

    @Throws(IOException::class)
    override fun deleteAll(path: String) {
        lruCache.invalidate()
    }

    override fun exists(path: String): Boolean {
        try {
            return lruCache.get(path) != null
        } catch (e: IOException) {
            throw Exceptions.propagate(e)
        }

    }

    @Throws(FileNotFoundException::class)
    override fun list(path: String): Collection<String> {
        throw UnsupportedOperationException()
    }

    override fun getRecordState(expirationUnit: TimeUnit, expirationDuration: Long, path: String): RecordState {
        return RecordState.FRESH
    }
}