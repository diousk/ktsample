package com.example.chenweiming.mykktapplication.utils

import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class AudoDisposeViewModel: ViewModel() {
    open val compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        compositeDisposable.dispose()
    }
}