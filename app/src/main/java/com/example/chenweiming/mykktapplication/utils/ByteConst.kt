package com.example.chenweiming.mykktapplication.utils

object ByteConst {
    const val BYTE = 1
    const val KB = 1024
    const val MB = 1024 * KB
}