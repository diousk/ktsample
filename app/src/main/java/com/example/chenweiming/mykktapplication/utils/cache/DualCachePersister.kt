package com.example.chenweiming.mykktapplication.utils.cache

import com.example.chenweiming.mykktapplication.repository.base.Timestamped
import com.example.chenweiming.mykktapplication.utils.logger.Logger
import com.nytimes.android.external.store3.base.Persister
import com.vincentbrison.openlibraries.android.dualcache.DualCache
import io.reactivex.Maybe
import io.reactivex.Single

class DualCachePersister<Parsed, Key>(
    private val dualCache: DualCache<Parsed>,
    private val logger: Logger? = null,
    private val expirationTimeout: Long = -1L
) : Persister<Parsed, Key>{

    override fun write(key: Key, parsed: Parsed): Single<Boolean> {
        return try {
            logger?.d("write: $key")
            if (parsed is Timestamped) {
                parsed.timestamp = System.currentTimeMillis()
                logger?.d("write: $key with time: ${parsed.timestamp}")
            }
            dualCache.put(key.toString(), parsed)
            logger?.d("write: $key success, disk byte used: ${dualCache.diskUsedInBytes}")
            Single.just(true)
        } catch (e: Exception) {
            logger?.d("write: $key failed, e:$e")
            Single.just(false)
        }
    }

    override fun read(key: Key): Maybe<Parsed> {
        return try {
            val parsed = dualCache.get(key.toString())
            if (parsed is Timestamped) {
                logger?.d("read: $key with time: ${parsed.timestamp}, timeout: $expirationTimeout")
            }

            return if (isExpired(parsed)) {
                logger?.d("expired: $key, timeout: $expirationTimeout")
                Maybe.empty<Parsed>()
            } else {
                logger?.d("read emit: $key")
                Maybe.just(parsed)
            }
        } catch (e: Exception) {
            logger?.d("read: $key failed, e:$e")
            Maybe.empty<Parsed>()
        }
    }

    private fun isExpired(parsed: Parsed): Boolean {
        if (expirationTimeout == -1L) {
            return false
        }

        if (parsed !is Timestamped) {
            return false
        }

        val current = System.currentTimeMillis()
        return (current - parsed.timestamp) > expirationTimeout
    }
}