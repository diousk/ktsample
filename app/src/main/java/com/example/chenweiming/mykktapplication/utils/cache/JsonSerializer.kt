package com.example.chenweiming.mykktapplication.utils.cache

import com.vincentbrison.openlibraries.android.dualcache.CacheSerializer
import java.io.IOException
import com.squareup.moshi.Moshi
import com.squareup.moshi.JsonAdapter
import java.lang.Exception


class JsonSerializer<T>
/**
 * Default constructor.
 * @param clazz is the class of object to serialize/deserialize.
 */
    (private val clazz: Class<T>, moshi: Moshi) : CacheSerializer<T> {
    private  var adapter: JsonAdapter<T> = moshi.adapter(clazz)

    override fun fromString(data: String): T {
        try {
            return adapter.fromJson(data)!!
        } catch (e: IOException) {
            e.printStackTrace()
        }

        throw IllegalStateException()
    }

    override fun toString(obj: T): String {
        try {
            return adapter.toJson(obj)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        throw IllegalStateException()
    }
}