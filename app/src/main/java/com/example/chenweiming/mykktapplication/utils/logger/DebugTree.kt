package com.example.chenweiming.mykktapplication.utils.logger

import android.os.Build
import android.util.Log
import timber.log.Timber

/**
 * Created by david.chen@soocii.me on 2018/11/8.
 */
class DebugTree: Timber.DebugTree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        if (Build.MANUFACTURER.equals("HUAWEI") || Build.MANUFACTURER.equals("samsung")) {
            when (priority) {
                Log.VERBOSE, Log.DEBUG, Log.INFO -> super.log(Log.ERROR, tag, message, t)
                else -> super.log(priority, tag, message, t)
            }
        } else {
            super.log(priority, tag, message, t)
        }
    }

    override fun createStackElementTag(element: StackTraceElement): String? {
        return super.createStackElementTag(element)
    }
}