package com.example.chenweiming.mykktapplication.utils.logger

import android.os.Build
import android.util.Log
import timber.log.Timber

class ReleaseTree: Timber.DebugTree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        if (priority == Log.VERBOSE || priority == Log.DEBUG || priority == Log.INFO) {
            return
        }

//        if (t == null) {
//            Crashlytics.logException(new Exception(message));
//        } else {
//            Crashlytics.logException(t);
//        }
        super.log(priority, tag, message, t)
    }

    override fun createStackElementTag(element: StackTraceElement): String? {
        return super.createStackElementTag(element)
    }
}