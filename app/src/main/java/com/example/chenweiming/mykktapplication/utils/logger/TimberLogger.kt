package com.example.chenweiming.mykktapplication.utils.logger

import timber.log.Timber
import javax.inject.Inject

/**
 * Created by david.chen@soocii.me on 2018/11/8.
 */
class TimberLogger @Inject constructor() : Logger {
    fun setup(debugMode: Boolean) {
        if (debugMode) {
            Timber.plant(DebugTree())
        } else {
            Timber.plant(ReleaseTree())
        }
    }

    override fun v(message: String, vararg args: Any) = Timber.tag(getCaller()).v(message, args)

    override fun v(t: Throwable, message: String, vararg args: Any) = Timber.tag(getCaller()).v(t, message, args)

    override fun v(t: Throwable) = Timber.tag(getCaller()).v(t)

    override fun d(message: String, vararg args: Any) = Timber.tag(getCaller()).d(message, args)

    override fun d(t: Throwable, message: String, vararg args: Any) = Timber.tag(getCaller()).d(t, message, args)

    override fun d(t: Throwable) = Timber.tag(getCaller()).d(t)

    override fun i(message: String, vararg args: Any) = Timber.tag(getCaller()).i(message, args)

    override fun i(t: Throwable, message: String, vararg args: Any) = Timber.tag(getCaller()).i(t, message, args)

    override fun i(t: Throwable) = Timber.tag(getCaller()).i(t)

    override fun w(message: String, vararg args: Any) = Timber.tag(getCaller()).w(message, args)

    override fun w(t: Throwable, message: String, vararg args: Any) = Timber.tag(getCaller()).w(t, message, args)

    override fun w(t: Throwable) = Timber.tag(getCaller()).w(t)

    override fun e(message: String, vararg args: Any) = Timber.tag(getCaller()).e(message, args)

    override fun e(t: Throwable, message: String, vararg args: Any) = Timber.tag(getCaller()).e(t, message, args)

    override fun e(t: Throwable) = Timber.tag(getCaller()).e(t)

    override fun wtf(message: String, vararg args: Any) = Timber.tag(getCaller()).wtf(message, args)

    override fun wtf(t: Throwable, message: String, vararg args: Any) = Timber.tag(getCaller()).wtf(t, message, args)

    override fun wtf(t: Throwable) = Timber.tag(getCaller()).wtf(t)

    private fun getCaller(): String {
        val st = Thread.currentThread().stackTrace ?: return "Unknown"

        var findSelf = false
        var info = "Unknown"
        for (e in st) {
            val name = e.className
            if (!findSelf) {
                if (name == TimberLogger::class.java.name) {
                    findSelf = true
                }
            } else {
                if (name != TimberLogger::class.java.name) {
                    val s = name.split("\\.".toRegex()).dropLastWhile{ it.isEmpty() }.toTypedArray()
                    info = s[s.size - 1]
                    break
                }
            }
        }

        return info
    }
}