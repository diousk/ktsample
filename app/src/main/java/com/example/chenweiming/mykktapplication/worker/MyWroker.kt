package com.example.chenweiming.mykktapplication.worker

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.work.Result
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.example.chenweiming.mykktapplication.App
import com.example.chenweiming.mykktapplication.const.PrefConst
import javax.inject.Inject
import javax.inject.Named

class MyWroker(private val context: Context, workerParameters: WorkerParameters)
    : Worker(context, workerParameters) {

    lateinit var pref: SharedPreferences

    override fun doWork(): Result {
        Log.d("MyWorker", "doWork on thread: ${Thread.currentThread().name}")
        Log.d("MyWorker", "prefs debuggable: ${pref.getBoolean(PrefConst.App.DEBUGGABLE, false)}")
        Log.d("MyWorker", "key1: ${inputData.getString("key1")}")
        Log.d("MyWorker", "key2: ${inputData.getInt("key2", 0)}")

        return Result.success()
    }
}